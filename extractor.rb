#!/usr/bin/env ruby

BADADDR = -9999999999999

def csvify(str)
  str.gsub(/"/, "\"\"")
end

# Helper stuff
class Interface
  attr_accessor :data, :info
  def initialize(fn)
    @f = File.open(fn, "rb")
    @info = {}
  end
  def read(size, addr=BADADDR)
    seek(addr)

    @f.read(size).unpack("C*")
  end
  def r8(addr=BADADDR)
    read(1, addr)[0]
  end
  def r8_nozero(addr=BADADDR)
    v = r8(addr)
    v = r8 while v == 0
    v
  end
  def r16(addr=BADADDR)
    v = r8(addr)

    (v << 8) | r8
  end
  def r32(addr=BADADDR)
    v = r16(addr)

    (v << 16) | r16
  end
  def rstr(addr=BADADDR)
    str = ""

    c = 0xFF
    seek(addr)
    while c != 0
      c = r8
      str.concat(c) if c != 0
    end

    str
  end
  def rstr_pascal(addr=BADADDR)
    str = ""

    seek(addr)
    len = r8
    for i in 0...len
      c = r8
      str.concat(c)
    end

    str
  end
  def skip_pascal(addr=BADADDR)
    seek(addr)
    len = r8
    seek(len, IO::SEEK_CUR)
  end
  def seek(addr, mode=IO::SEEK_SET)
    @f.seek(addr, mode) if addr != BADADDR
  end
  def tell
    @f.tell
  end
end

class QuizAndDragons
  attr_accessor :categories, :wordlist

  # Special single-character word encodings
  TINY_ENCODE_MAP = [
    0x00,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,
    0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x21,0x22,0x23,0x24,0x00,
    0x25,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x26,0x00,0x27,0x28,
    0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,
    0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,
    0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,
    0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,
  ]

  def initialize(intf)
    @intf = intf
    @intf.data = self

    @wordlist = []
    @categories = []
  end
  def add_info(hash)
    @intf.info.merge!(hash)
  end
  def load
    load_wordlist
    load_categories
  end
  def load_wordlist
    @wordlist = []
    @intf.seek(@intf.info[:data_addr])
    for i in 0...(0x800+TINY_ENCODE_MAP[0xBF])
      @wordlist.push(@intf.rstr_pascal)
    end
  end
  def load_categories
    for i in 0...@intf.info[:cat_cnt]
      cat = Category.new(@intf, self)
      cat.load(i)
      @categories.push(cat)
    end
  end

  def get_string(idx, casechg=0)
    str = @wordlist[idx].dup
    if casechg != 0
      str_a = str.slice!(0)
      str_a.swapcase!
      str.prepend(str_a)
    end
    str
  end
  def decode_single_char(c, str, is_q)
    mode = 0
    code = 0
    if c == 0 # End of Question
      mode = -1
    elsif c == 1 # End of Answer/Newline
      if not is_q
        mode = -1
      else
        str = str.chomp(" ") + " "
      end
    elsif c <= 0xBF # Small character
      if TINY_ENCODE_MAP[c.ord] == 0 # Unencoded character
        str.concat(c)
      else # Tiny encoded character
        c = TINY_ENCODE_MAP[c.ord] - 1
        str += get_string(c)
      end
    else # Long encoded character
      code = c << 8
      mode = 1
    end

    return mode,str,code
  end
  def decode_long_char(code, str)
    code_str = code & 0x7FF # Actual string code
    code_spacepfx = code & 0x2000 # Prefixed space
    code_spacesfx = code & 0x1000 # Suffixed space
    code_casechg = code & 0x800 # Change the casing of the string

    code_str += TINY_ENCODE_MAP[0xBF] # code 0 is the first code unencodable with tiny encoding
    str += " " if code_spacepfx != 0
    str += get_string(code_str, code_casechg)
    str += " " if code_spacesfx != 0

    str
  end
  # Decode a full string
  def decode(data, is_q)
    str = ""
    len = 0
    mode = 0
    code = 0
    for c in data
      len += 1
      if mode == 0 # Normal character processing state
        mode,str,code = decode_single_char(c, str, is_q)
      elsif mode == 1 # Long encoded character processing state
        str = decode_long_char(code|c, str)
        mode = 0
      end

      break if mode == -1
    end

    return str,len
  end
  
  def wordlist_to_csv
    str = ""
    @wordlist.each_with_index do |word,idx|
      str += sprintf "0x%03X,\"%s\"\n", idx, csvify(word)
    end

    str
  end
  def to_csv
    str = ""
    for cat in @categories
      str += cat.to_csv
    end

    str
  end
  def to_s
    str = ""
    for cat in @categories
      str += cat.to_s
    end

    str+"\n"
  end
end

class Category
  attr_accessor :addr, :name
  def initialize(intf, qad)
    @qad = qad
    @intf = intf
    @name = ""
    @addr = BADADDR
    @endaddr = BADADDR
    @questions = []
  end
  def load(idx)
    load_name(idx)
    load_ptr(idx)
    load_questions(idx)
  end
  def load_name(idx)
    name_addr = @intf.info[:cat_addr] + @intf.info[:cat_cnt]*4 # Names start after pointer table
    @intf.seek(name_addr)

    for i in 0...idx # Munch the strings we don't care about
      @intf.rstr
    end
    @name = @intf.rstr
  end
  def load_ptr(idx)
    ptr_addr = @intf.info[:cat_addr] + (idx*4)
    @addr = @intf.r32(ptr_addr) + @intf.info[:data_addr] # The addresses in the table are actually offsets
    if idx != @intf.info[:cat_cnt]-1
      @endaddr = @intf.r32(ptr_addr+4) + @intf.info[:data_addr] # End address is start of next category
    else
      @endaddr = 0x80000 # Or the end of the program if it's the last category
    end
  end
  def load_questions(idx)
    adr = @addr
    @intf.seek(adr)
    while adr < @endaddr
      q = Question.new(@intf, @qad, self)
      adr = q.load
      @questions.push(q)
   end
  end

  def to_csv
    str = ""
    for q in @questions
      str += q.to_csv
    end

    str
  end
  def to_s
    str = ""
    str += sprintf "Category '%s': @ $%06X\n", @name, @addr
    for q in @questions
      str += q.to_s
    end

    str+"\n"
  end
end

class Question
  def initialize(intf, qad, cat)
    @cat = cat
    @qad = qad
    @intf = intf
    @addr = BADADDR
    @size = 0
    @q = ""
    @answers = []
  end
  def load
    @addr = @intf.tell
    @size = @intf.r8_nozero - 1
    @data = @intf.read(@size)
    interpret

    @intf.tell
  end
  def interpret
    @q = decode_one(true)
    for i in 0...4
      @answers[i] = decode_one(false)
    end
  end
  def decode_one(is_q)
    str,len = @intf.data.decode(@data, is_q)
    @data = @data.drop(len)

    str
  end

  def to_csv
    str = ""
    str += sprintf "\"%s\",\"%s\"", csvify(@cat.name), csvify(@q)
    for i in 0...4
      str += sprintf ",\"%s\"", csvify(@answers[i])
    end
    str += "\n"

    str
  end
  def to_s
    str = ""
    str += sprintf "\tQuestion: '%s': @ $%06X\n", @q, @addr
    for i in 0...4
      str += sprintf "\t\tAnswer %d: '%s'\n", i+1, @answers[i]
    end

    str+"\n"
  end
end

progbin = "qad.bin"
datacsv = "question.csv"
wordcsv = "words.csv"

if ARGV.count < 1
  printf "Usage:\n\t%s qad.bin [question.csv [words.csv]]\n", $0
  exit 1
end

progbin = ARGV[0] if ARGV.count > 0
datacsv = ARGV[1] if ARGV.count > 1
wordcsv = ARGV[2] if ARGV.count > 2

qad = QuizAndDragons.new(Interface.new(progbin))
qad.add_info(cat_cnt: 14, cat_addr: 0x1DBE6) # This address found at $5B52
qad.add_info(data_addr: 0x256EE) # This address found at $5B5A, and $5CD0
qad.load

File.open(wordcsv, "wb").write(qad.wordlist_to_csv)
File.open(datacsv, "wb").write(qad.to_csv)

exit 0
